<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Larakit\Boot;
use Larakit\TraitKernel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;

class Kernel extends HttpKernel
{
    public function __construct(Application $app, Router $router) {
        $this->middleware       = Boot::middlewares();
        $this->routeMiddleware  = Boot::middlewares_route();
        $this->middlewareGroups = Boot::middlewares_group();
        parent::__construct($app, $router);
    }

    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [];
}