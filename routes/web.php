<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

$routes = rglob('*.php', 0, __DIR__ . '/web/');
foreach($routes as $route) {
    include $route;
}

Route::get('/', function () {
    return view('welcome');
});
